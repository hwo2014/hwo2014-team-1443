package hwo2014

import RaceModelData._



class RaceState(val track: TrackDescription, val cars: Vector[CarPos], val myCarName: String) {
  val myCar = cars.filter(c => c.car.name == myCarName)(0)
}