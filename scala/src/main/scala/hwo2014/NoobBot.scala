package hwo2014

import org.json4s._
import org.json4s.native.JsonMethods._
import org.json4s.DefaultFormats

import org.json4s.JsonDSL._
import java.net.Socket
import java.io.{BufferedReader, InputStreamReader, OutputStreamWriter, PrintWriter}
import org.json4s.native.Serialization
import scala.annotation.tailrec

import RaceModelData._

object NoobBot extends App {
  args.toList match {
    case hostName :: port :: botName :: botKey :: _ =>
      new NoobBot(hostName, Integer.parseInt(port), botName, botKey)
    case _ => println("args missing")
  }
}

class NoobBot(host: String, port: Int, botName: String, botKey: String) {
  implicit val formats = new DefaultFormats{}
  val socket = new Socket(host, port)
  val writer = new PrintWriter(new OutputStreamWriter(socket.getOutputStream, "UTF8"))
  val reader = new BufferedReader(new InputStreamReader(socket.getInputStream, "UTF8"))
  var trackDescription: TrackDescription = null
  var prevRaceState: RaceState = null
  var currRaceState: RaceState = null
  var turboAvailable = false
  sendJoin(botName, botKey)
  //sendJoinRace(botName, botKey, "usa", 1)
  play

  @tailrec private def play { 
    val line = reader.readLine()
    if (line != null) {
      val json = parse(line)
      val messageType = extractMessageType(json)
      println(messageType)
      messageType match {
        case Join => sendPing()
        case YourCar => sendPing()
        case GameInit => {
          trackDescription = new TrackDescription(json); sendPing()
        }
        case GameStart => sendPing()
        case CarPositions => {
          if (prevRaceState == null) sendThrottle(1.0)
          if (currRaceState != null) prevRaceState = currRaceState
          currRaceState = new RaceState(trackDescription, extractCarPositions(json), botName)
          //////////////////////////////////////////////////////////////////////////////////////////
          //main logic should be concentrated here
          if (prevRaceState != null) {
            //println(currRaceState.cars)
            sendThrottle(0.6)
          }
          //////////////////////////////////////////////////////////////////////////////////////////
        }
        case TurboAvailable => {
          turboAvailable = true
          sendPing()
        }
        case Crash => {
          sendPing()
        }
        case Spawn => {
          sendPing()
        }
        case GameEnd => sendPing()
        case TournamentEnd => sendPing()
        case Finish => sendPing()
        case Dnf => println("disqualified :" + line)
        case _ => sendPing()
      }

      play
    }
  }

  def send(msg: String): Unit =  {
    writer.println(msg)
    writer.flush()
  }

  def sendPing(): Unit = {
    val json =  ("msgType" -> "ping")
    send(compact(render(json)))
  }

  def sendJoin(botName: String, botKey: String): Unit = {
    val json = ("msgType" -> "join") ~ ("data" -> ("name" -> botName) ~ ("key" -> botKey))
    send(compact(render(json)))
  }

  def sendJoinRace(botName: String, botKey: String, trackName: String, carCount: Int): Unit = {
    val json = ("msgType" -> "joinRace") ~
      ("data" ->
        (("botId" -> (("name" -> botName) ~ ("key" -> botKey))) ~
          ("trackName" -> trackName) ~
          ("carCount" -> carCount)))
    send(compact(render(json)))
  }

  def sendThrottle(throttle: Double): Unit = {
    val json = ("msgType" -> "throttle") ~ ("data" -> throttle)
    send(compact(render(json)))
  }

  def sendSwitchLane(dir: Direction): Unit = {
    val json = ("msgType" -> "switchLane") ~ ("data" -> (if (dir == ToLeft) "Left" else "Right"))
    send(compact(render(json)))
  }

  def sendTurbo(): Unit = {
    turboAvailable = false
    val json = ("msgType" -> "turbo") ~ ("data" -> "my turbo message")
    send(compact(render(json)))
  }

}
