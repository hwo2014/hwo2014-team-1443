package hwo2014

import org.json4s.native.Serialization
import org.json4s.NoTypeHints
import org.json4s.native.Serialization.{read, write}

import org.json4s._
import org.json4s.native.JsonMethods._
import org.json4s.JsonDSL._





object RaceModelData {
  implicit val formats = Serialization.formats(NoTypeHints)

  abstract class Direction
  case object ToLeft extends Direction
  case object ToRight extends Direction

  abstract class MessageType
  case object YourCar extends MessageType
  case object GameInit extends MessageType
  case object GameStart extends MessageType
  case object CarPositions extends MessageType
  case object GameEnd extends MessageType
  case object TournamentEnd extends MessageType
  case object Crash extends MessageType
  case object Spawn extends MessageType
  case object LapFinished extends MessageType
  case object Dnf extends MessageType
  case object Finish extends MessageType
  case object TurboAvailable extends MessageType
  case object Join extends MessageType
  case object JoinRace extends MessageType
  case object UnknownMessage extends MessageType


  def extractMessageType(json: JValue): MessageType = {
    val v = json \ "msgType"
    v match {
      case JString("yourCar") => YourCar
      case JString("gameInit") => GameInit
      case JString("gameStart") => GameStart
      case JString("carPositions") => CarPositions
      case JString("gameEnd") => GameEnd
      case JString("tournamentEnd") => TournamentEnd
      case JString("crash") => Crash
      case JString("spawn") => Spawn
      case JString("lapFinished") => LapFinished
      case JString("dnf") => Dnf
      case JString("finish") => Finish
      case JString("turboAvailable") => TurboAvailable
      case JString("join") => Join
      case JString("joinRace") => JoinRace
      case _ => UnknownMessage
    }
  }

  case class CarId(name: String, color: String)

  def extractMyCarId(json: JValue): CarId = {
    if (extractMessageType(json) == YourCar) {
      val JString(name) = json \ "data" \ "name"
      val JString(color) =  json \ "data" \ "color"
      CarId(name, color)
    }
    else throw new RuntimeException("Not YourCar Message")
  }

  //Here goes track description elements
  case class RaceSession(durationMs: Option[Double], laps: Option[Int], maxLapTimeMs: Option[Double], quickRace: Option[Boolean]) {
    def isQuickRace: Boolean = quickRace match {case None => false; case _ => true}
    def isQualification: Boolean = durationMs match {case Some(_) => true; case _ => false}
    def isRace: Boolean = !isQuickRace
  }

  case class PieceOfTrack(length: Option[Double], switch: Option[Boolean], radius: Option[Double], angle: Option[Double]) {
    def canSwitch: Boolean = switch match {case Some(_) => true; case None => false}
    def getLength: Double = {
      length match {
        case Some(x) => x
        case None => radius.getOrElse(0.0)*angle.getOrElse(0.0)*math.Pi/180.0
      }
    }
  }

  case class CarPos(car: CarId, angle: Double, pieceIndex: Int, inPieceDistance: Double, startLaneIndex: Int, endLaneIndex: Int)

  class TrackDescription(json: JValue) {
    assert(extractMessageType(json) == GameInit)
    val laneDistances = extractLaneData(json)
    val numOfLanes = laneDistances.length
    val piecesOfTrack = extractPiecesOfTrack(json)
    val numOfPieces = piecesOfTrack.length
    val cars = extractCars(json)
    val numOfCars = cars.length
    val raceSession = extractRaceSession(json)
  }


  def extractLaneData(json: JValue): Vector[Double] = {
    case class LaneData(distanceFromCenter: Double, index: Int)
    val json1 = json \ "data" \ "race" \ "track" \ "lanes"
    val lanes = json1.extract[List[LaneData]]
    val lanesWillBe = Array.fill(lanes.length)(0.0)
    for (i <- lanes) {
      lanesWillBe(i.index) = i.distanceFromCenter
    }
    lanesWillBe.toVector
  }

  def extractPiecesOfTrack(json: JValue): Vector[PieceOfTrack] = {
    val json1 = json \ "data" \ "race" \ "track" \ "pieces"
    json1.extract[List[PieceOfTrack]].toVector
  }

  def extractCars(json: JValue): Vector[CarId] = {
    val json1 = json \ "data" \ "race" \ "cars" \ "id"
    json1 match {
      case j: JObject => Vector(j.extract[CarId])
      case j: JArray => j.extract[List[CarId]].toVector
      case _ => throw new RuntimeException("bad input data")
    }
  }

  def extractRaceSession(json: JValue): RaceSession = {
    val json1 = json \ "data" \ "race" \ "raceSession"
    json1.extract[RaceSession]
  }

  def extractCarPositions(json: JValue): Vector[CarPos] = {
    val carIds = (json \ "data" \"id") match {
      case a: JObject => Vector(a.extract[CarId])
      case a: JArray => a.extract [List[CarId]].toVector
      case _ => throw new RuntimeException("bad input data")
    }
    val angles = (json \ "data" \"angle") match {
      case a: JDouble => Vector(a.extract[Double])
      case a: JArray => a.extract [List[Double]].toVector
      case _ => throw new RuntimeException("bad input data")
    }
    val pieceIndices = (json \ "data" \"piecePosition" \ "pieceIndex") match {
      case a: JInt => Vector(a.extract[Int])
      case a: JArray => a.extract [List[Int]].toVector
      case _ => throw new RuntimeException("bad input data")
    }
    val inPieceDistances = (json \ "data" \"piecePosition" \ "inPieceDistance") match {
      case a: JDouble => Vector(a.extract[Double])
      case a: JArray => a.extract [List[Double]].toVector
      case _ => throw new RuntimeException("bad input data")
    }
    val startLaneIndices = (json \ "data" \"piecePosition" \ "lane" \ "startLaneIndex") match {
      case a: JInt => Vector(a.extract[Int])
      case a: JArray => a.extract [List[Int]].toVector
      case _ => throw new RuntimeException("bad input data")
    }
    val endLaneIndices = (json \ "data" \"piecePosition" \ "lane" \ "endLaneIndex") match {
      case a: JInt => Vector(a.extract[Int])
      case a: JArray => a.extract [List[Int]].toVector
      case _ => throw new RuntimeException("bad input data")
    }
    val almostFinal = for (i <- 0 until carIds.length) yield CarPos(carIds(i), angles(i), pieceIndices(i), inPieceDistances(i), startLaneIndices(i), endLaneIndices(i))
    almostFinal.toVector
  }

  def main(args: Array[String]): Unit = {
    val line = """|
                 |{"msgType":"gameInit","data":{"race":{"track":{"id":"keimola","name":"Keimola","pieces":[{"length":100.0},{"length":100.0},{"length":100.0},{"length":100.0,"switch":true},{"radius":100,"angle":45.0},{"radius":100,"angle":45.0},{"radius":100,"angle":45.0},{"radius":100,"angle":45.0},{"radius":200,"angle":22.5,"switch":true},{"length":100.0},{"length":100.0},{"radius":200,"angle":-22.5},{"length":100.0},{"length":100.0,"switch":true},{"radius":100,"angle":-45.0},{"radius":100,"angle":-45.0},{"radius":100,"angle":-45.0},{"radius":100,"angle":-45.0},{"length":100.0,"switch":true},{"radius":100,"angle":45.0},{"radius":100,"angle":45.0},{"radius":100,"angle":45.0},{"radius":100,"angle":45.0},{"radius":200,"angle":22.5},{"radius":200,"angle":-22.5},{"length":100.0,"switch":true},{"radius":100,"angle":45.0},{"radius":100,"angle":45.0},{"length":62.0},{"radius":100,"angle":-45.0,"switch":true},{"radius":100,"angle":-45.0},{"radius":100,"angle":45.0},{"radius":100,"angle":45.0},{"radius":100,"angle":45.0},{"radius":100,"angle":45.0},{"length":100.0,"switch":true},{"length":100.0},{"length":100.0},{"length":100.0},{"length":90.0}],"lanes":[{"distanceFromCenter":-10,"index":0},{"distanceFromCenter":10,"index":1}],"startingPoint":{"position":{"x":-300.0,"y":-44.0},"angle":90.0}},"cars":[{"id":{"name":"TaroKong","color":"red"},"dimensions":{"length":40.0,"width":20.0,"guideFlagPosition":10.0}}],"raceSession":{"laps":3,"maxLapTimeMs":60000,"quickRace":true}}},"gameId":"a29afdec-b99e-4fb3-8302-647095648221"}
                 |""".stripMargin

    val json = parse(line)
    val c = new TrackDescription(json)
    println(c.cars)
  }
}